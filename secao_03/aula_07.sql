DROP DATABASE banco;

DROP DATABASE hcode;

CREATE DATABASE hcode;

CREATE TABLE tb_funcionarios (
	id INT,
    nome VARCHAR(100),
    salario DECIMAL(10,2),
    admissao DATE,
    sexo ENUM('F', 'M'),
    cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP()
);

INSERT INTO tb_funcionarios VALUES(1, 'Pedro Henrique', 4999.99, '2016-01-01', 'M', NULL);

SELECT * FROM tb_funcionarios;