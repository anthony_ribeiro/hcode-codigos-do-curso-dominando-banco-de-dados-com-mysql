# Hcode - Códigos do Curso Dominando Banco de Dados com MySQL

Sejam bem-vindos! Esse repositório é destinado para os alunos da Hcode.
Nele, é possível baixar todos os códigos desenvolvidos em nosso curso Dominando Banco de Dados com MySQL.
Você pode abrir esses arquivos em qualquer editor de texto ou em seu Banco de Dados.
Bons estudos :)